-- This file is part of point-table, a Lua library
-- Copyright (C) 2017  Aidan Gauland
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public
-- License along with this program.  If not, see
-- <http://www.gnu.org/licenses/>.

-- A point table is indexed by points of either the form {x,y} or {x = x1, y = y1}
-- and is converted to a key of the form "x,y" by the indexing metamethods.

local lume = require "lume"
local inspect = require "inspect"

local exports = {}

-- Accepts points in either of the forms {x,y} or {x = x1, y = y1}
local function tableToKey(k)
  local function raiseError(msg)
    error(msg, 4)
  end

  -- Input sanity check
  if type(k) ~= "table" then
    raiseError("Invalid key type: " .. type(k))
  elseif table.maxn(k) == 2 then
    if not (type(k[1]) == "number" and type(k[2]) == "number") then
      raiseError("Invalid key: " .. inspect(k))
    else
      return table.concat(k, ",")
    end
  elseif table.maxn(k) == 0 then
    if not (type(k.x) == "number" and type(k.y) == "number") then
      raiseError("Invalid key: " .. inspect(k))
    else
      return k.x .. "," .. k.y
    end
  else
    raiseError("Invalid key: " .. inspect(k))
  end
end

local function idx(t, k)
  return rawget(t, tableToKey(k))
end

local function newidx(t, k, v)
  rawset(t, tableToKey(k), v)
end

function exports.makePointTable()
  return setmetatable({}, {__index = idx, __newindex = newidx})
end

-- Converts a key of the form "x,y" to a table of the form {x = x, y = y}
function exports.keyToTable(key)
  local items = lume.split(key, ",")
  return {x = tonumber(items[1]), y = tonumber(items[2])}
end

return exports
